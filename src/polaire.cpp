
#include "polaire.hpp"
#include "cartesien.hpp"
#include <cmath>


Polaire::Polaire(double const teta, double const r) :
	m_teta(teta),
	m_r(r)
{
}

Polaire::Polaire(Cartesien const & c) :
	Polaire()
{
	c.convertir(*this);
}


Polaire::Polaire() :
	Polaire(0., 0.)
{
}

void Polaire::convertir(Point & point) const {
	Cartesien* c = dynamic_cast <Cartesien*> (&point);
	if (c != nullptr) {
		(*c).setX(m_r * cos(m_teta * M_PI / 180.0));
		(*c).setY(m_r * sin(m_teta * M_PI / 180.0));
	} else {
		Polaire* p = static_cast <Polaire*> (&point);
		(*p).setAngle(m_teta);
		(*p).setDistance(m_r);
	}
}


void Polaire::afficher(std::ostream& os) const {
	os << "(a=" << m_teta << ";d=" << m_r << ")";
}
