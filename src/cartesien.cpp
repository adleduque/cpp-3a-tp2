
#include "cartesien.hpp"
#include "polaire.hpp"
#include <cmath>

Cartesien::Cartesien(double const x, double const y) :
	m_x(x),
	m_y(y)
{
}

Cartesien::Cartesien() :
	Cartesien(0., 0.)
{
}

Cartesien::Cartesien(Polaire const & p) :
	Cartesien()
{
	p.convertir(*this);
}


void Cartesien::convertir(Point& point) const {
	Polaire * p = dynamic_cast <Polaire*> (&point);
	if (p != nullptr) {
		(*p).setDistance(sqrt(m_x*m_x + m_y*m_y));
		(*p).setAngle(atan(m_y/m_x) * 180.0 / M_PI);
	} else {
		Cartesien* c = static_cast <Cartesien*> (&point);
		(*c).setX(m_x);
		(*c).setY(m_y);
	}
}

void Cartesien::afficher(std::ostream& os) const {
	os << "(x=" << m_x << ";y=" << m_y << ")";
}
