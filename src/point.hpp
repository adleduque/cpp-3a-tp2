
#ifndef POINT_HPP
#define POINT_HPP

#include <iostream>

class Point {
public :
	virtual void afficher(std::ostream&) const = 0;
	virtual void convertir(Point &) const = 0;
};

std::ostream& operator <<(std::ostream& os, Point const & p);

#endif
