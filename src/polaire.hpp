
#ifndef POLAIRE_HPP
#define POLAIRE_HPP

#include "point.hpp"
#include "cartesien.hpp"

class Cartesien;

class Polaire : public Point {
private:
	double m_teta;
	double m_r;

public:
	Polaire(double const r, double const teta);
	Polaire(Cartesien const & c);
	Polaire();

	void convertir(Point & p) const;
	void afficher(std::ostream& os) const override;

	inline double const getDistance() const { return m_r; }

	inline double const getAngle() const { return m_teta; }

	inline void setDistance(double const r) { m_r = r; }

	inline void setAngle(double const teta) { m_teta = teta; }

};


#endif