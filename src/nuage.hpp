
#ifndef NUAGE_HPP
#define NUAGE_HPP

#include <vector>
#include "cartesien.hpp"

template<typename T>
class Nuage {
private:
	std::vector<T> vect;

public:
	int size() const;
	void ajouter(T const & t);

	using const_iterator = typename std::vector<T>::iterator;
	const_iterator begin();
	const_iterator end();

};

template<typename T>
int Nuage<T>::size() const {
	return vect.size();
}

template<typename T>
void Nuage<T>::ajouter(T const & t) {
	vect.push_back(t);
}

template<typename T>
typename Nuage<T>::const_iterator Nuage<T>::begin() {
	return vect.begin();
}

template<typename T>
typename Nuage<T>::const_iterator Nuage<T>::end() {
	return vect.end();
}

template<typename T>
T barycentre_v1(Nuage<T> & n) {
	double x = 0;
	double y = 0;
	double taille = n.size();
	if (taille != 0) {
		typename Nuage<T>::const_iterator it;
		for(it = n.begin(); it != n.end(); ++it) {
			Cartesien tmp(*it);
			x += (tmp).getX();
			y += (tmp).getY();
		}
		return T(Cartesien(x/taille, y/taille));
	}
	return T(0,0);
}

#endif