
#ifndef CARTESIEN_HPP
#define CARTESIEN_HPP

#include <iostream>
#include "point.hpp"
#include "polaire.hpp"


class Polaire;

class Cartesien : public Point {
private:
	double m_x;
	double m_y;
public:
	Cartesien(double const x, double const y);
	Cartesien();
	Cartesien(Polaire const & p);
	
	void convertir(Point& p) const;
	void afficher(std::ostream& os) const override;

	double const getX() const { return m_x; }
	double const getY() const { return m_y; }

	void setX(double const x) { m_x = x; }
	void setY(double const y) { m_y = y; }
};


#endif